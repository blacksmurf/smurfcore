SMURFCORE
=============
SMURFCORE est un projet d'un OS de base typé "cloud"


Procédure d'installation
------------------------
```
$ wget https://gitlab.com/blacksmurf/smurfcore/raw/master/cloud-config.yml && sudo ros install -c cloud-config.yml -d /dev/sda
```

Prérequis
---------
- ?

Compatibilité
-------------
- GNU/Linux

Licence
-------
Ce produit est sous licence Creative Commons : Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
Elle prévoit la copie et la distribution sur n'importe quel support dans des conditions de gratuité,
et l'interdiction de la revente, de l'appropriation ou de la modification des sources de ce projet.

